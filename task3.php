<?php
abstract class customFile
{
	protected $_fileName;
	abstract public function processing();
	static public function instance($fileName)
	{
		$fileExt=pathinfo($fileName, PATHINFO_EXTENSION);
		$className=$fileExt."File";
		if (in_array($className, get_declared_classes())==true)
			{
				if (is_subclass_of($className, __CLASS__))
					{
						$instance=new $className($fileName);
					}
				else
					{
						throw new Exception($className." must be subclass of ".__CLASS__);
					}
			}
		else 
			{
				throw new Exception("Undefined class ".$className);
			}
		return $instance;
	}
	
	protected function __construct($fileName)
	{
		$this->_fileName=$fileName;
	}
}

class docFile extends customFile
{
	public function processing()
	{
		echo __METHOD__."\r\n";
	}
}

class iniFile extends customFile
{
	public function processing()
	{
		echo __METHOD__."\r\n";
	}
}

class txtFile extends customFile
{
	public function processing()
	{
		echo __METHOD__."\r\n";
	}
}

class xmlFile extends customFile
{
	public function processing()
	{
		echo __METHOD__."\r\n";
	}
}

$fileName="file-name.xml";
customFile::instance($fileName)->processing();
